#include "src/graph/graph.hpp"
#include <stdio.h>

#include <boost/lambda/lambda.hpp>
#include <boost/test/included/unit_test.hpp>
#include <iostream>
#include <iterator>
#include <algorithm>

int main(){
    Graph g = Graph();
    
    for(int k=1; k<=4; k=k+1){
        std::string id = "n" + std::to_string(k);
        g.addNodeById(id);
    }

    g.connectById("n1", "n2");
    g.connectById("n1", "n3");
    g.connectById("n2", "n3");
    g.connectById("n2", "n4");
    g.connectById("n4", "n4");
    
    g.print();

    Node source = g.getNodeById("n1");
    g.dfs(source);
}

