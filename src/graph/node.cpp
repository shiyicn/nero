#include "node.hpp"

Node::Node(std::string& id){
    this->id = id;
};

Node::Node(){
    this->id = std::string("TMP_ID");
}

void Node::link(Node& n){
    this->neighbors.push_back(n);
};

void Node::print(){
    printf("Node id: %s \n", this->id.c_str());
    printf("Neighbors: ");
    for(auto& it: this->neighbors){
        printf("%s ", it.id.c_str());
    }
    printf("\n");
};

template <> struct std::hash<Node>{
    size_t operator()(const Node& n) const{
        std::hash<std::string> hasher;
        std::size_t h = hasher(n.id);
        return h;
    }
};
