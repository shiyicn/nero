#include "node.hpp"
#include <unordered_map>

class Graph{
    public:
        Node source;
        std::unordered_map<std::string, Node> nodeById;
    public:
        Graph();
        void addNodeById(std::string& id);
        Node getNodeById(std::string& id);
        Node getNodeById(const char* id);
        void print();
        void connectById(std::string& id1, std::string& id2);
        void connectById(const char* id1, const char* id2);
        void dfs(Node& source);
        void bfs(Node& source);
        std::unordered_map<std::string, int> inDegree();
        void tsort();
};

