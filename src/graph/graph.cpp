#include "graph.hpp"
#include <unordered_set>

Graph::Graph() {

};

void Graph::addNodeById(std::string& id){
    if(this->nodeById.find(id) == this->nodeById.end()){
        printf("Create Node with id: %s \n", id.c_str());
        Node n(id);
        this->nodeById[id] = n;
    }else{
        printf("Id: $s already exists in graph.", id.c_str());
    }
}

void Graph::print(){
    printf("Graph with Nodes: \n");
    for(auto& it: this->nodeById){
        it.second.print();
    }
}

Node Graph::getNodeById(std::string& id){
    if(this->nodeById.find(id) == this->nodeById.end()){
        printf("Node id: %s not in graph.", id.c_str());
        return Node();
    }else{
        return this->nodeById[id];
    }
}

Node Graph::getNodeById(const char* id){
    std::string idStr = std::string(id);
    return this->getNodeById(idStr);
}

void Graph::dfs(Node& source){
    std::unordered_set<std::string> visitedNodeId;
    std::vector<std::string> nodeIdToVisit;
    nodeIdToVisit.push_back(source.id);
    
    std::string id;
    Node n;

    while(!nodeIdToVisit.empty()){
        id = nodeIdToVisit.back();
        visitedNodeId.insert(id);
        n = this->getNodeById(id);
        printf("Node %s ->", id.c_str());
        nodeIdToVisit.pop_back();
        for(auto& neighbor: n.neighbors){
            if(visitedNodeId.find(neighbor.id) == visitedNodeId.end()){
                nodeIdToVisit.push_back(neighbor.id);
            }
        }
    }
    printf("\n");
}

void Graph::connectById(std::string& id1, std::string& id2){
    if (this->nodeById.find(id1) != this->nodeById.end() && this->nodeById.find(id2) != this->nodeById.end()){
        this->nodeById[id1].link(this->nodeById[id2]);
    } else {
        printf("id: %s or %s not in graph", id1.c_str(), id2.c_str());
    }
}

void Graph::connectById(const char* id1, const char* id2){
    std::string id1str = std::string(id1);
    std::string id2str = std::string(id2);
    this->connectById(id1str, id2str);
}

void Graph::bfs(Node& source){}

std::unordered_map<std::string, int> Graph::inDegree(){
    std::unordered_map<std::string, int> res;
    
    for (auto& it: this->nodeById){
        if (res.find(it.second.id) == res.end()){
            res[it.second.id] = 0;
        }
        for (auto& n: it.second.neighbors){
            if (res.find(n.id) == res.end()){
                res[n.id] = 0;
            }else{
                res[n.id] += 1;
            }
        }
    }

    return res;
}

void Graph::tsort(){
    std::unordered_map<std::string, int> deg = this->inDegree();
    // for 
};
