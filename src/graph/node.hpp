#include <vector>
#include <iostream>
#include <string>

class Node{
    
    public:
        std::string id;
        std::vector<Node> neighbors;

    public:
        Node(std::string& id);
        Node();
        void link(Node& n);
        void print();
};

template <> struct std::hash<Node>;
