main: main.o node.o graph.o
	g++ -o main main.o graph.o node.o

main.o: main.cpp
	g++ -c main.cpp

graph.o: src/graph/graph.cpp src/graph/graph.hpp
	g++ -c src/graph/graph.cpp

node.o: src/graph/node.cpp src/graph/graph.hpp
	g++ -c src/graph/node.cpp

clean:
	-rm *.o main
